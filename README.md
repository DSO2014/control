# Control multithread en C#

Veremos la implementación de un sistema de control concurrente usando threads en Linux.
Planificaremos los threads para que se ejecuten con prioridades de Tiempo Real.
Para ello utilizaremos las funciones ya conocidas para leer temperaturas de sensores DS18B20 y nuevas funciones disponibles en threads.c que nos permitirán manejar los threads que vamos a programar. Tenemos funciones para crear un thread a partir de una función, para establecer la prioridad de tiempo real del thread actual (hacerlo dentro de la función que ejecute ese thread), y funciones para manejar variables de condición que usaremos para sincronizar el funcionamiento de nuestros threads. Por ejemplo para que el thread que decide que hacer con las lecturas de temperatura, se espere hasta que haya nuevos datos disponibles del sensor...

### FUNCIONES DISPONIBLES: ###
```C
// crea un nuevo thread que ejecuta la función
pthread_t * inicia_thread(void* (*funcion)());
// Espera a que termine el thread indicado
void espera_thread (pthread_t * th);
// pone la prioridad a Tiempo Real con valor n
void establecer_prioridad(int n);
// crea una nueva condicion con valor val
tipo_condicion * inicia_condicion( int val);
// espera a que la condición se cumpa (1)
void espera_condicion(tipo_condicion *cond);
// espera a que la condición se cumpa (1) y la pone a 0
void espera_condicion_reset(tipo_condicion *cond);
// pone la condición a 1 o 0
void actualiza_condicion(tipo_condicion *cond, int val);
```

## Ejercicio ##
Disponemos de un programa inicial llamado control.c, que espera la pulsación de los botones de la placa auxiliar de E/S y pone en marcha o detiene al thread que hace sonar la alarma a través del altavoz de nuestra placa.
A partir de aquí habrá que implementar el sistema que se muestra en la imagen disponible en formato PDF (esquema_aplicacion_iind.pdf)

En la aplicación resultante tendremos 5 threads que colaboran para implementar el sistema de control. 

El sistema debe implementar la siguiente funcionalidad:

* Periódicamente el thread "leer" leerá una temperatura del sensor y se almacenará en la variable global "temperatura". * Activando la condición "sensor" para que el thread "decidir", que debe estar esperando a este evento,  analice la situación y actúe.
* El thread "decidir" será el encargado de comparar la temperatura con la temperatura crítica y activar la variable alarma. Activará la condición "datos" para que el thread "visualizar" muestre todas las variables del sistema por pantalla y la temperatura sea representada usando los leds de nuestra placa de E/S. El thread "decidir" teniendo en cuenta la activación de la alarma y el estado del altavoz (on/off = 1/0) activará o desactivará la condición "sonido" para que el thread "sonar" haga sonar la alarma o se detenga.
* Por otro lado hemos instalado dos interrupciones que vigilan la pulsación de los botones. Cuando se lanzan las interrupciones se anota que botón se ha pulsado y se activa la condición "boton"
Cuando la condición "boton" se activa, el thread "entrada" analiza qué botón se ha pulsado mediante las variable "boton1" y "boton2"
* Si se ha pulsado el botón 1 se debe cambiar el estado del altavoz, conmutando entre 0 y 1 (off / on)
* Si se ha pulsado el botón 2 se debe modificar la temperatura crítica (por ejemplo incrementándola hasta llegar a un máximo donde vuelva a resetearse a el valor mínimo).
* Sea cual sea el botón pulsado, deberíamos activar la condición "sensor" para que el thread "decidir" revalue la situación.

### How do I get set up? ###

* Para compilar usar la herramienta make:
```bash
make
```

### Who do I talk to? ###

* Andrés Rodríguez (andres@uma.es)