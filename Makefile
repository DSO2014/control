MOSQUITTO_DIR=../mosquitto/lib
CJSON_DIR=../cjson

CFLAGS= -I $(MOSQUITTO_DIR) -I $(CJSON_DIR) 

LDFLAGS= -pthread -lrt -lm -lwiringPi
LDFLAGS+= -L $(MOSQUITTO_DIR) -lmosquitto
LDFLAGS+= -L $(CJSON_DIR) -lcjson

EJECUTABLES= control

.PHONY: programas clean librerias

programas : $(EJECUTABLES)

librerias : 
	make -C $(MOSQUITTO_DIR)
	make -C $(CJSON_DIR)

% : %.c
	gcc -o $@ $^ $(CFLAGS) $(LDFLAGS)

clean :  
	rm $(EJECUTABLES) 




